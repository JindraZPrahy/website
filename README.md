# Website

This is just a simple example of how you can host a website through git-lab.

Here you can see the website:
[https://asz-it-course.gitlab.io/website/](https://asz-it-course.gitlab.io/website/)

To deploy your own website, simply create an analogous repository with the
same files as I have and than wait a few minutes for GitLab continuous
integration to do its job.

Or you can watch [this tutorial](https://www.youtube.com/watch?v=cERdbQ-GgOo&ab_channel=ProgrammingLiftoff)
to see this process done in greater detail.
